# README #

* This Extention is compatible with Opencart 2.2.0.0_b1
* This Extention using  braintree version 2.32.0


# SETUP #

* Copy folder **admin**, **catalog** and **vender** into your **upload** opencart folder
* Activate the extension thru your Opencart Admin Panel.
* The extension uses the Braintree API. You need to enter either **Sandbox** or **Production** **Public/Private API keys** and the **Merchant ID** on the extension settings page.
* In order to obtain this information please login to your Braintree account dashboard, click on **Account** in the top right corner of the page, then **API Keys**.
* When switching to **Production mode** please replace **API keys** and **Merchant ID** with Production ones and change **Transaction Mode** setting to **Production**. You're all set to accept credit card payments thru Braintree.

# Enabling AVS and CVV rules #
1. Log into the Control Panel (Braintree website)
1. Navigate to Settings > Processing > Basic Credit Card Fraud Tools > AVS or CVV
1. Click Edit
1. Select your desired AVS or CVV rejection criteria
1. You have the option to apply the rules to all transactions or only to specific card types, amounts, or merchant accounts
1. Click Save

* If you use our Drop-in UI and you enable CVV rules or AVS rules for postal codes in the Control Panel, the fields needed to collect that information will automatically appear on your checkout form.